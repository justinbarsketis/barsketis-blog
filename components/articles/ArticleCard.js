import { Flex, Box, Text, Image, Link } from "@chakra-ui/react"

const ArticleCard = ({
  published_at,
  title,
  excerpt,
  primary_tag,
  primary_author,
  slug,
}) => {
  console.log(primary_tag)
  // get only yyyy-mm-dd from published_at
  const date = published_at.split("T")[0]
  console.log(primary_author)
  return (
    <Flex
      bg="gray.700"
      // _dark={{
      //   bg: "#3e3e3e",
      // }}
      p={25}
      w="full"
      alignItems="center"
      justifyContent="center"
    >
      <Box
        mx="auto"
        px={8}
        py={4}
        rounded="lg"
        shadow="lg"
        // bg="white"
        _dark={{
          bg: "gray.800",
        }}
        maxW="2xl"
      >
        <Flex justifyContent="space-between" alignItems="center">
          <Text
            fontSize="sm"
            color="gray.600"
            _dark={{
              color: "gray.400",
            }}
          >
            {date}
          </Text>
          <Link
            href={primary_tag?.url}
            px={3}
            py={1}
            bg="gray.600"
            color="gray.100"
            fontSize="sm"
            fontWeight="700"
            rounded="md"
            _hover={{
              bg: "gray.500",
            }}
          >
            {primary_tag?.name}
          </Link>
        </Flex>

        <Box mt={2}>
          <Link
            fontSize="2xl"
            color="gray.700"
            href={`${process.env.NEXT_PUBLIC_BLOG_URL}/${slug}`}
            _dark={{
              color: "white",
            }}
            fontWeight="700"
            _hover={{
              color: "gray.600",
              _dark: {
                color: "gray.200",
              },
              textDecor: "underline",
            }}
          >
            {title}
          </Link>
          <Text
            mt={2}
            color="gray.600"
            _dark={{
              color: "gray.300",
            }}
          >
            {excerpt}
          </Text>
        </Box>

        <Flex justifyContent="space-between" alignItems="center" mt={4}>
          <Link
            color="brand.600"
            _dark={{
              color: "brand.400",
            }}
            _hover={{
              textDecor: "underline",
            }}
            href={`${process.env.NEXT_PUBLIC_BLOG_URL}/${slug}`}
          >
            Read more
          </Link>

          <Flex alignItems="center">
            <Image
              mx={4}
              w={10}
              h={10}
              rounded="full"
              fit="cover"
              display={{
                base: "none",
                sm: "block",
              }}
              src={primary_author?.profile_image}
              alt="avatar"
            />
            <Link
              href={primary_author?.url}
              color="gray.700"
              _dark={{
                color: "gray.200",
              }}
              fontWeight="700"
              cursor="pointer"
            >
              {primary_author?.name}
            </Link>
          </Flex>
        </Flex>
      </Box>
    </Flex>
  )
}
export default ArticleCard
