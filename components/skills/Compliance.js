import React from 'react'

import {
  Flex,
  Center,
  Square,
  Box,
  Text,
  Heading,
  Container,
  VStack,
  Tabs,
  TabList,
  TabPanels,
  Tab,
  TabPanel,
  Image,
  AspectRatio
} from '@chakra-ui/react'

const Compliance = () => {
  return (
    <Container maxW={'full'}>
      <Flex pt={75} pb={75} pl={15} pr={15} bg="rgb(249, 250, 251)">
        <Text color={'black'}>Hippa</Text>
        <Text color={'black'}>GDPR</Text>
      </Flex>
    </Container>
  )
}

export default Compliance
