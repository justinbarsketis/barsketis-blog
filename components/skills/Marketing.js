import React from 'react'

import {
  Flex,
  Center,
  Square,
  Box,
  Text,
  Heading,
  Container,
  VStack,
  Tabs,
  TabList,
  TabPanels,
  Tab,
  TabPanel,
  Image,
  AspectRatio
} from '@chakra-ui/react'

const Marketing = () => {
  return (
    <Container maxW={'full'}>
      <Flex pt={75} pb={75} pl={15} pr={15} bg="rgb(249, 250, 251)">
        <Text color={'black'}>Marketing</Text>
      </Flex>
    </Container>
  )
}

export default Marketing

// Marketing
// Work closely with my marketing partners to
// Create Web UI Designs
// Custom Logos and Branding Materials
// Content Marketing - Blog post from a professional writer to help keep your audiences entertained
// Social Media Management -
