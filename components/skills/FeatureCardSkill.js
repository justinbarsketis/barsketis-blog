import React from 'react'
import {
  Flex,
  Center,
  Square,
  Box,
  Text,
  Heading,
  Container,
  VStack,
  Link
} from '@chakra-ui/react'

const FeatureCardSkill = ({ IconBrand, title, description }) => {
  return (
    <Container
      boxShadow="xl"
      borderRadius="10"
      mb="25"
      bg="white"
      maxW={'95%'}
      _hover={{
        background: 'gray.50'
      }}
    >
      <Link>
        <Flex>
          <Box display={'flex'} flex={2} color={'gray.900'} fontSize={32}>
            <Center>{IconBrand}</Center>
          </Box>
          <Box flex={10}>
            <Text pl={2} pt={2} fontSize={14} color={'gray.900'}>
              {title}
            </Text>
            <Text p={2} fontSize={11} color={'gray.900'}>
              {description}
            </Text>
          </Box>
        </Flex>
      </Link>
    </Container>
  )
}

export default FeatureCardSkill
