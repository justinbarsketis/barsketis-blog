import React from 'react'
import {
  Flex,
  Center,
  Square,
  Box,
  Text,
  Heading,
  Container,
  VStack,
  Tabs,
  TabList,
  TabPanels,
  Tab,
  TabPanel,
  Image,
  AspectRatio
} from '@chakra-ui/react'
import {
  SiAmazonaws,
  SiSupabase,
  SiFirebase,
  SiRedwoodjs,
  SiShopify
} from 'react-icons/si'

const ProviderTab = ({ logo, name }) => {
  return (
    <Flex>
      <Box mr={2} fontSize={24}>
        {logo}
      </Box>
      <Text>{name}</Text>
    </Flex>
  )
}
const ProviderTabContent = ({ title, description, video, logo }) => {
  return (
    <Box display="flex">
      <Center flex="1">
        <Text fontSize={32}>{logo}</Text>
        <Heading>{title}</Heading>
      </Center>
      <Box flex="1">
        <AspectRatio flex="1" maxW="100%" ratio={16 / 9}>
          <iframe title="naruto" src={video} allowFullScreen />
        </AspectRatio>
      </Box>
    </Box>
  )
}
const SystemDesign = () => {
  return (
    <Container maxW={'full'}>
      <Flex pt={75} pb={75} pl={15} pr={15}>
        <VStack align="stretch">
          <Center>
            <Heading>System Design</Heading>
          </Center>
          <Center>
            <Heading size="sm" p={15}>
              Putting it all together
            </Heading>
          </Center>
          <Center>
            <Text textAlign={'center'} maxW={'60%'}>
              Picking your backend providers can be a daunting task. Allow me to
              break your paralysis analysis and recommend tried and true systems
              to get you moving forward.
            </Text>
          </Center>
        </VStack>
      </Flex>
      <Tabs isFitted variant="enclosed">
        {/* //SiAmazonaws, SiSupabase, SiFirebase, SiRedwoodjs */}
        <TabList mb="1em">
          <Tab>
            <ProviderTab logo={<SiAmazonaws />} name={'AWS'} />
          </Tab>
          <Tab>
            <ProviderTab logo={<SiSupabase />} name={'Supabase'} />
          </Tab>
          <Tab>
            <ProviderTab logo={<SiFirebase />} name={'Firebase'} />
          </Tab>
          <Tab>
            <ProviderTab logo={<SiRedwoodjs />} name={'Redwood'} />
          </Tab>
          <Tab>
            <ProviderTab logo={<SiShopify />} name={'Shopify'} />
          </Tab>
        </TabList>
        <TabPanels>
          <TabPanel>
            <ProviderTabContent
              video={'https://www.youtube.com/embed/QhBnZ6NPOY0'}
              title={'AWS'}
              description={'test'}
              logo={<SiAmazonaws />}
            />
          </TabPanel>
          <TabPanel>
            <ProviderTabContent
              video={'https://www.youtube.com/embed/QhBnZ6NPOY0'}
              title={'Supabase'}
              description={'test'}
              logo={<SiSupabase />}
            />
          </TabPanel>
          <TabPanel>
            <ProviderTabContent
              video={'https://www.youtube.com/embed/QhBnZ6NPOY0'}
              title={'Firebase'}
              description={'test'}
              logo={<SiFirebase />}
            />
          </TabPanel>
          <TabPanel>
            <ProviderTabContent
              video={'https://www.youtube.com/embed/QhBnZ6NPOY0'}
              title={'Redwood'}
              description={'test'}
              logo={<SiRedwoodjs />}
            />
          </TabPanel>
          <TabPanel>
            <ProviderTabContent
              video={'https://www.youtube.com/embed/QhBnZ6NPOY0'}
              title={'Shopify'}
              description={'test'}
              logo={<SiShopify />}
            />
          </TabPanel>
        </TabPanels>
      </Tabs>
    </Container>
  )
}

export default SystemDesign

{
  /* <Heading>System Design</Heading>
<Text>
  Designing your backend structure can be a daunting task as you analyze
  the never ending javascript frameworks that come out daily. Allow me to
  break your paralysis analysis and recommend you tried and true systems
  to get you moving forward and ensure your backend scales with the
  success of your project.
</Text>
<List>
  <ListItem>
    AWS Certified - AWS is quickly taking over the market for their
    reasonable prices for small companies, but also the ability to
    infinitely scale. Most companies choose their infrastructire. I am a
    certified AWS expert that can help you integrate your entire backend
    on aws, including but not limited to Cognito, DynamoDB / RDS, Route
    53, Amplify, CloudFront, and private VPC networks.
  </ListItem>
  <ListItem>
    Supabase and Firebase - Great for teams that want to get off the
    ground running with an easier architecture. Both platforms offer
    abstracted and centralized services to be able to quickly scale your
    platform quickly.
  </ListItem>
  <ListItem>
    Redwood and Blitz - Opinionated frameworks that help you structure
    your project for immediate development. You will be able to hire even
    novice developers after implementing these libraries due to their
    strict structure.
  </ListItem>
</List> */
}
