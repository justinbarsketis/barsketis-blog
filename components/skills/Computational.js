import React from 'react'
import { Heading, Text, ListItem, List } from '@chakra-ui/react'
import {
  Flex,
  Center,
  Square,
  Box,
  Text,
  Heading,
  Container,
  VStack,
  Tabs,
  TabList,
  TabPanels,
  Tab,
  TabPanel,
  Image,
  AspectRatio
} from '@chakra-ui/react'

const Computational = () => {
  return (
    <Container maxW={'full'}>
      <Flex pt={75} pb={75} pl={15} pr={15} bg="rgb(249, 250, 251)">
        <Heading>Computational & Crypto</Heading>
        <Text>
          Just need help integrating your database and API? Allow me to come in
          and help you achieve an efficient and secure system.
        </Text>
        <List>
          <ListItem>
            Trading Algorithms - Highly competent mathematician that can put
            your math into code to test new ideas.
          </ListItem>
          <ListItem>
            Data Scraping - Use chromium or xx to help you scrape data into a
            database or
          </ListItem>
          <ListItem>
            Solidity and Ethereum - Can implement smart contracts and token
            networks to help you into the world of crypto.
          </ListItem>
        </List>
      </Flex>
    </Container>
  )
}

export default Computational
