import React from 'react'
import {
  Flex,
  Center,
  Square,
  Box,
  Text,
  Heading,
  Container,
  VStack,
  SimpleGrid
} from '@chakra-ui/react'
import FeatureCardSkill from './FeatureCardSkill'

const FeatureCard = ({ title, subTitle, description, skills, flip }) => {
  if (flip) {
    return (
      <Container maxW={'full'}>
        <Flex pt={75} pb={75} pl={15} pr={15} bg="rgb(249, 250, 251)">
          <Box flex="2">
            <SimpleGrid columns={2}>
              {skills.map((item) => (
                <FeatureCardSkill
                  IconBrand={item.icon}
                  title={item.title}
                  description={item.description}
                />
              ))}
            </SimpleGrid>
          </Box>
          <Box p={25} flex="1">
            <Heading color={'gray.900'} size="lg">
              {title}
            </Heading>
            <Heading color={'teal.400'} mb={15} mt={15} size="md">
              {subTitle}
            </Heading>
            <Text fontSize={14} color={'gray.900'}>
              {description}
            </Text>
          </Box>
        </Flex>
      </Container>
    )
  } else {
    return (
      <Container maxW={'full'}>
        <Flex pt={75} pb={55} pl={15} pr={15} bg="rgb(249, 250, 251)">
          <Box p={25} flex="1">
            <Heading color={'gray.900'} size="lg">
              {title}
            </Heading>
            <Heading color={'teal.400'} mb={15} mt={15} size="md">
              {subTitle}
            </Heading>
            <Text fontSize={14} color={'gray.900'}>
              {description}
            </Text>
          </Box>
          <Box flex="2">
            <SimpleGrid columns={2}>
              {skills.map((item) => (
                <FeatureCardSkill
                  IconBrand={item.icon}
                  title={item.title}
                  description={item.description}
                />
              ))}
            </SimpleGrid>
          </Box>
        </Flex>
      </Container>
    )
  }
}

export default FeatureCard
