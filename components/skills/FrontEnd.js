import {
  Text,
  List,
  ListItem,
  Heading,
  SimpleGrid,
  Box
} from '@chakra-ui/react'
import FeatureCard from './FeatureCard'

import {
  SiReact,
  SiChakraui,
  SiRedux,
  SiNextdotjs,
  SiTypescript
} from 'react-icons/si'
import { MdOutlinePhoneIphone } from 'react-icons/md'

const FrontEnd = () => {
  const frontText =
    'React.js is an opensource platform developed by facebook used to make fast modern interfaces for your users to interact with.'
  const skills = [
    {
      title: 'React',
      description:
        'React.js is an opensource platform developed by facebook used to make fast modern interfaces for your users to interact with.',
      icon: <SiReact />
    },
    {
      title: 'Typescript',
      description:
        'React.js is an opensource platform developed by facebook used to make fast modern interfaces for your users to interact with.',
      icon: <SiTypescript />
    },
    {
      title: 'React Native',
      description:
        'React.js is an opensource platform developed by facebook used to make fast modern interfaces for your users to interact with.',
      icon: <MdOutlinePhoneIphone />
    },
    {
      title: 'Next',
      description:
        'NextJS.js will handle server side caching of your website to optimize it better for SEO and also offer great performance improvements.',
      icon: <SiNextdotjs />
    },
    {
      title: 'Redux & Context',
      description:
        'I can implement complex data stores to properly set up your front end to integrate seamlessly with your api and scale as your project becomes more complex',
      icon: <SiRedux />
    },
    {
      title: 'UI Libraries',
      description:
        'Bootstrap, Tailwinds, Material UI or Chakra UI will help you have a consistent design structure throughout the entire project and provide beautiful components.',
      icon: <SiChakraui />
    }
  ]

  return (
    <>
      {/* New Hero Slider */}
      <Box borderBottom="1px solid" borderColor="rgb(226, 232, 240)">
        <FeatureCard
          title="Front End"
          subTitle="Modern Frameworks for future success"
          description={frontText}
          skills={skills}
        />
      </Box>
    </>
  )
}

export default FrontEnd
