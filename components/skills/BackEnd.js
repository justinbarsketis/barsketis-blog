import React from 'react'
import { Text, List, ListItem, Heading, SimpleGrid } from '@chakra-ui/react'
import FeatureCard from './FeatureCard'

import {
  SiExpress,
  SiNestjs,
  SiGo,
  SiApollographql,
  SiPrisma,
  SiAuth0
} from 'react-icons/si'

const BackEnd = () => {
  const frontText =
    'Integrating your database and API? Allow me to come in and help you achieve an efficient and secure system.'
  const skills = [
    {
      title: 'Express',
      description:
        'Express / Fastify - Core backend services for most API these days.',
      icon: <SiExpress />
    },
    {
      title: 'NestJs',
      description:
        'Bringing MVC models to React to help you structure your request to better scale in the future.',
      icon: <SiNestjs />
    },
    {
      title: 'Go',
      description:
        'Great for high volume apis where efficiency and low latency is crucial.',
      icon: <SiGo />
    },
    {
      title: 'Apollo / GrahQl',
      description:
        'The new and improved method overtaking REST apis for their felxibility and speed',
      icon: <SiApollographql />
    },
    {
      title: 'Prisma',
      description: 'Make fewer errors with this typescript ORM',
      icon: <SiPrisma />
    },
    {
      title: 'Authentication',
      description:
        'Intergrate common authentication libraries like Auth0,PassportJs, and OAuth',
      icon: <SiAuth0 />
    }
  ]

  return (
    <>
      {/* New Hero Slider */}
      <FeatureCard
        title="Back End"
        subTitle="Efficient, secure, and fast backend system"
        description={frontText}
        skills={skills}
        flip
      />
    </>
  )
}

export default BackEnd
