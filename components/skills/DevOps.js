import React from 'react'

import {
  Flex,
  Center,
  Square,
  Box,
  Text,
  Heading,
  Container,
  VStack,
  Tabs,
  TabList,
  TabPanels,
  Tab,
  TabPanel,
  Image,
  AspectRatio
} from '@chakra-ui/react'

const DevOps = () => {
  return (
    <Container maxW={'full'}>
      <Flex pt={75} pb={75} pl={15} pr={15}>
        <Text>Dev Ops</Text>
      </Flex>
    </Container>
  )
}

export default DevOps
