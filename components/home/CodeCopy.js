import React, { useState } from 'react'
import { useClipboard, Flex, Button, Input } from '@chakra-ui/react'

const CodeCopy = ({ code }) => {
  const [codeValue, setCodeValue] = useState(code)
  const { hasCopied, onCopy } = useClipboard(codeValue)

  return (
    <>
      <Flex mb={2}>
        <Input value={codeValue} isReadOnly placeholder="Welcome" />
        <Button onClick={onCopy} ml={2}>
          {hasCopied ? 'Copied' : 'Copy'}
        </Button>
      </Flex>
    </>
  )
}
export default CodeCopy
