import React from 'react'
import {
  Flex,
  Box,
  Center,
  Spacer,
  Container,
  Text,
  Image,
  Code,
  Button,
  Heading
} from '@chakra-ui/react'
import { ChevronRightIcon } from '@chakra-ui/icons'
const BioCard = () => {
  return (
    <Container
      boxShadow="2xl"
      borderRadius="10"
      mb="50"
      bg="white"
      maxW={'4xl'}
    >
      <Flex>
        <Box
          flex="1"
          borderRight="1px solid"
          ml="3"
          borderColor="rgb(226, 232, 240)"
        >
          <Heading size="md" pt="25" pb="25" color="black">
            Get to know me
          </Heading>
          <Text fontSize="12" pr="15" pb="2" color="gray.900">
            I am flexible developer with over 10 years of experience. If you
            have a unique stack you want to work with you don't see below, dont
            be afraid to ask me about it.
          </Text>
          <Button
            mt="2"
            bg={'rgb(226, 232, 240)'}
            rounded={'5'}
            color="gray.900"
            fontSize="13"
            px={4}
            _hover={{
              bg: 'white'
            }}
          >
            Read about my life <ChevronRightIcon />
          </Button>
        </Box>
        <Box display="flex" border="2px" flex="1">
          <Center flex="1">
            <Image
              borderRadius="full"
              w="60%"
              boxShadow={'2xl'}
              src="/Justin-Barsketis-Headshot.jpg"
              alt="Justin Barsketis Headshot"
            />
          </Center>
        </Box>
        <Box
          borderLeft="1px solid"
          borderColor="rgb(226, 232, 240)"
          p="25"
          flex="1"
        >
          <Heading size="md" color="black">
            Job Query
          </Heading>
          <Code fontSize="13" whiteSpace="pre-line" color="gray.800">
            {`{
            'Home Bases': 'Salt Lake City, Utah && San Miguel, Mexico'
            'Location': 'Full Remote'
            'Contract': 'Salary || Contract'
            'Hourly Rate': '$100 an hour'
            'Job Role': 'Software Engineer'
            }`}
          </Code>
        </Box>
      </Flex>
    </Container>
  )
}

export default BioCard
