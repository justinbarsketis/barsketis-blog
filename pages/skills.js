import React from 'react'
import Head from 'next/head'
import Image from 'next/image'
import Header from '../components/Header'
import Footer from '../components/Footer'
import FrontEnd from '../components/skills/FrontEnd'
import BackEnd from '../components/skills/BackEnd'
import SystemDesign from '../components/skills/SystemDesign'
import Marketing from '../components/skills/Marketing'
import DevOps from '../components/skills/DevOps'
import Compliance from '../components/skills/Compliance'
import Hero from '../components/skills/Hero'
const Skills = () => {
  return (
    <div>
      <Head>
        <title>Justin Skill Set</title>
        <meta name="description" content="Justin Barsketis Blog" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />

      <main style={{ flexGrow: '1' }}>
        <Hero />
        <FrontEnd />
        <BackEnd />
        <SystemDesign />
        <DevOps />
        <Compliance />
        <Marketing />
        {/* <Skills /> */}
        {/* <Skills /> */}
      </main>

      <footer>
        <Footer />
      </footer>
    </div>
  )
}

export default Skills

// I am a flexible developer with over 10 years of experience, if you have a unique stack you want to work with, dont be afraid to ask me. This is just my personal preference.
// Front End
// UI Libraies - I can work with BootStrap, Tailwinds, Material UI or Chakra UI for component libraries. This will help you have a consistent design structure throughout the entire project and provide beautiful components.
//
// A Simple site React, Typescript or even Vanilla Javscript, NextJS. Able to implement complex designs, use data stores, and hydrate pages from your backend
// Mobile Apps - Proficient in React Native to make you a cross platform application
// SEO - 10+ years in digital marketing, I will ensure your website is SEO optimized to start your path towards SEO Dominance. Free SEO Audits included.
// Backend Designs - Implement a database of your choice and an API to make your application functional.

// System Design -
// All AWS - Prepare your company for future scaling
// Supabase - Quick to set up an manage for smaller dev teams
// Express, NestJS,
// Go API - For people needing the best performance for highly trafficked api's
// Apollo + GrapqhQl
// Resume Download, Contact Information
// Compliance - Structure your system to be hippa or gdpr compliant.

// Dev Ops
// VPS - Digital Ocean, Heroku
// Vercel
// AWS
// Kubernets and Docker
// Lambda and CloudFront
// Gatsby
// Wordpress - Security etc
// Server Pilot + VPS

// Marketing
// Work closely with my marketing partners to
// Create Web UI Designs
// Custom Logos and Branding Materials
// Content Marketing - Blog post from a professional writer to help keep your audiences entertained
// Social Media Management -

// Great Font https://eumray.com/

// Front End
// System Design
// Dev Ops
//
