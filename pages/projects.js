import Head from "next/head"
import Image from "next/image"
import { Heading, Link, Flex, Box } from "@chakra-ui/react"
import Header from "../components/Header"
import ProjectSection from "../components/projects/ProjectSection"

export default function Projects() {
  return (
    <div>
      <Head>
        <title>Justin Barsketis Projects</title>
        <meta name="description" content="Justin Barsketis Blog" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />

      <main style={{ flexGrow: "1" }}>
        <ProjectSection />
      </main>

      <footer>{/* <Footer /> */}</footer>
    </div>
  )
}
