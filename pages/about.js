import Head from 'next/head'
import Image from 'next/image'
import { Heading, Link, Flex, Box } from '@chakra-ui/react'
import Header from '../components/Header'

export default function About() {
  return (
    <div>
      <Head>
        <title>Welcome to Articles</title>
        <meta name="description" content="Justin Barsketis Blog" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />

      <main style={{ flexGrow: '1' }}></main>

      <footer>{/* <Footer /> */}</footer>
    </div>
  )
}
