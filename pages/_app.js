// import '../styles/globals.css'

import { ChakraProvider } from "@chakra-ui/react"

import { extendTheme } from "@chakra-ui/react"
import { mode } from "@chakra-ui/theme-tools"
import "@fontsource/poppins/400.css"
import "@fontsource/poppins/700.css"
import "@fontsource/inter/400.css"
import "@fontsource/inter/700.css"

// 2. Extend the theme to include custom colors, fonts, etc
const config = {
  initialColorMode: "dark",
  useSystemColorMode: false,
}

const theme = extendTheme({
  config,
  a: {
    color: "teal.500",
  },

  fonts: {
    heading: "Poppins, sans-serif",
    body: "Inter, sans-serif",
    color: "",
  },
  brand: {
    900: "#1a365d",
    800: "#153e75",
    700: "#2a69ac",
  },
})

// Links #38b2ac light blue
//

function MyApp({ Component, pageProps }) {
  return (
    <ChakraProvider theme={theme}>
      <Component {...pageProps} />
    </ChakraProvider>
  )
}
export default MyApp

// npm i @fontsource/poppins
