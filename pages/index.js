import Head from "next/head"
import Image from "next/image"

import Header from "../components/Header"
import HeroCTA from "../components/home/HeroCTA"

import Footer from "../components/Footer"
import BioCard from "../components/home/BioCard"

export default function Home() {
  return (
    <div>
      <Head>
        <title>Welcome to Justin.Barsketis</title>
        <meta name="description" content="Justin Barsketis Blog" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />

      <main style={{ flexGrow: "1" }}>
        <HeroCTA />
        <BioCard />
        {/* <Skills /> */}
        {/* <Skills /> */}
      </main>

      <footer>
        <Footer />
      </footer>
    </div>
  )
}
