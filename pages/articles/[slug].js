import GhostContentAPI from "@tryghost/content-api"

const Article = ({ post }) => {
  return (
    <div>
      <h1>{post.title}</h1>
      <div dangerouslySetInnerHTML={{ __html: post.html }} />
    </div>
  )
}

export async function getStaticPaths() {
  const api = new GhostContentAPI({
    url: "https://grow.barsketis.com",
    key: process.env.NEXT_PUBLIC_CONTENT_API_KEY,
    version: "v5.0",
  })
  const posts = await api.posts.browse({
    limit: "all",
  })
  const paths = posts.map((post) => ({
    params: { slug: post.slug },
  }))

  return { paths, fallback: false }
}

export async function getStaticProps({ params }) {
  const api = new GhostContentAPI({
    url: "https://grow.barsketis.com",
    key: process.env.NEXT_PUBLIC_CONTENT_API_KEY,
    version: "v5.0",
  })
  const post = await api.posts.read({
    slug: params.slug,
  })

  if (!post) {
    return {
      notFound: true,
    }
  }

  return {
    props: { post },
  }
}

export default Article
