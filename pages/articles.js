import Head from "next/head"
import Image from "next/image"
import { Heading, Link, Flex, Box, Text } from "@chakra-ui/react"
import Header from "../components/Header"
import ArticleCard from "../components/articles/ArticleCard"
import GhostContentAPI from "@tryghost/content-api"
import { useEffect, useState } from "react"

import Footer from "../components/Footer"

export default function Articles() {
  const api = new GhostContentAPI({
    url: "https://grow.barsketis.com",
    key: process.env.NEXT_PUBLIC_CONTENT_API_KEY,
    version: "v5.0",
  })

  const [posts, setPosts] = useState([])
  useEffect(() => {
    api.posts
      .browse({
        limit: "all",
        include: "tags,authors",
      })
      .then((posts) => {
        console.log("posts", posts)
        setPosts(posts)
      })
      .catch((err) => {
        console.error(err)
      })
  }, [])

  return (
    <div>
      <Head>
        <title>Welcome to Articles</title>
        <meta name="description" content="Justin Barsketis Blog" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />

      <main style={{ flexGrow: "1" }}>
        <Heading as="h1" size="2xl" textAlign="center" my="4">
          Articles by Justin Barsketis
        </Heading>
        <Flex
          direction="column"
          align="center"
          justify="center"
          wrap="wrap"
          w="100%"
          p="4"
        >
          <Text align="center" p={10}>
            I use Ghost as my CMS and Next.js as my frontend framework.
            <br />
            <Link href="https://grow.barsketis.com">grow.barsketis.com</Link>
            <br />
            Here are some of my most recent articles
          </Text>

          {posts.map((post) => (
            <ArticleCard
              key={post.id}
              title={post.title}
              slug={post.slug}
              excerpt={post.excerpt}
              // feature_image={post.feature_image}
              primary_author={post.primary_author}
              primary_tag={post.primary_tag}
              published_at={post.published_at}
            />
          ))}
        </Flex>
      </main>

      <footer>{/* <Footer /> */}</footer>
    </div>
  )
}

// getStaticProps
